<?php

namespace Porra\Storage\Results;

use Results;

/**
 * Class used as a wrapper for the Eloquent model redefining the public methods used in the
 * Controllers to access the model data for Results.
 *
 * Class EloquentResultsRepository
 * @package Porra\Storage\Results
 */
class EloquentResultsRepository implements InterfaceResultsRepository {


    public function all()
    {
        return Results::all();
    }

    public function calculatePoints($player_id)
    {
        return Results::calculatePoints($player_id);
    }

    public function getComingMatches()
    {
        return Results::getComingMatches();
    }

    public function updateResultLeagueMatches($value,$id)
    {
        return Results::updateResultLeagueMatches($value,$id);
    }
}