<?php

namespace Porra\Storage\Players;

use Players;

/**
 * Class used as a wrapper for the Eloquent model redefining the public methods used in the
 * Controllers to access the model data for Players.
 *
 * Class EloquentPlayersRepository
 * @package Porra\Storage\Players
 */
class EloquentPlayersRepository implements InterfacePlayersRepository {


    public function all()
    {
        return Players::all();
    }

    public function calculatePoints($player)
    {
        return Players::calculatePoints($player);
    }
}