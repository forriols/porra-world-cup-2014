<?php

class Players extends Eloquent {

    protected $fillable = array('name','email');

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

}
