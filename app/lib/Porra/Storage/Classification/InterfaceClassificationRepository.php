<?php

namespace Porra\Storage\Classification;

interface InterfaceClassificationRepository {

    public function all();

    public function update($input);
}