<?php

Route::get('/',[
    'uses'  =>  'ClassificationController@showClassification',
    'as'    =>  'classification.home'
]);

Route::get('update',[
    'uses'  =>  'ClassificationController@updateClassification',
    'as'    =>  'classification.update'
]);

Route::get('user/{id}',[
    'uses'  =>  'ClassificationController@showUser',
    'as'    =>  'classification.user'
]);

Route::get('new-result',[
    'uses'  =>  'ResultsController@newResult',
    'as'    =>  'results.create'
]);

Route::post('new-result',[
    'uses'  =>  'ResultsController@storeResult',
    'as'    =>  'results.store'
]);

//ERROR REDIRECT
App::missing(function($exception){
    return Redirect::route('classification.home');
});