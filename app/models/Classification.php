<?php

class Classification extends Eloquent {

    protected $fillable   = array('points','last_position','current_position','fk_id_users');

    /**
     * we don't want automatic date update values in update operations
     *
     * @var bool
     */
    public    $timestamps = false;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'classification';

    /**
     * Function to warn that the classification's primary key is called different than id.
     *
     * @return string
     */
    public function getKeyName()
    {
        return "fk_id_users";
    }

    /**
     * This method get the classification from the table called classification and then
     * we get the position number trying to get the same position for the players who have
     * the same number of points.
     *
     * @return mixed
     */
    private static function calculatePositions()
    {
        $current_values = array();
        $classification = DB::table('classification')->orderBy('points', 'desc')->get();

        foreach($classification as &$player) {
            if (empty($current_values)) {
                $current_values['position'] = 1;
                $current_values['points']   = $player->points;
            }elseif ($player->points < $current_values['points']) {
                $current_values['position'] ++;
                $current_values['points']   = $player->points;
            }
            $player->position = $current_values['position'];
        }
        return $classification;
    }

    /**
     * Saving current positions for each player
     */
    public static function updateCurrentPositions()
    {
        foreach (self::calculatePositions() as $classification_row) {
            $prepare_classification                   = Classification::find($classification_row->fk_id_users);
            $prepare_classification->current_position = $classification_row->position;
            $prepare_classification->save();
        }
    }

    /**
     * We recalculate all the points for the classificaation. We get the points per each $player and than
     * we save its value to the main classificattion table.
     *
     * @param Players $players
     * @param Results $results
     */
    public static function updatePoints(Players $players, Results $results)
    {
        foreach ($players->all() as $player) {
            $prepare_player           = Classification::find($player->id);
            $prepare_player->points   = $results->calculatePoints($player->id);
            $prepare_player->save();
        }
    }

    /**
     *
     *
     * Copy values from current_position to last_position.
     *
     */
    public static function updateLastPositions()
    {
        DB::update('update classification set last_position = current_position');
    }

    /**
     * Listing the main classification
     *
     * @return mixed
     */
    public static function getClassification()
    {
        return DB::table('classification')
            ->join('users','classification.fk_id_users','=','users.id')
            ->select('classification.points','classification.current_position','classification.last_position',
                'users.id','users.name')
            ->orderBy('points','desc')
            ->get();
    }

    /**
    /* defining relationship between different tables
     */
    public function user()
    {
        return $this->hasOne('User');
    }
}
