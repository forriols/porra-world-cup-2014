@extends('layouts.master')
@section('content')
<div class=" panel panel-default col-lg-6 col-md-6 col-sm-6 col-xs-12">
    <div class="panel-heading">Classificació</div>

    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>&nbsp;</th>
                <th>Nom</th>
                <th>Punts</th>
            </tr>
        </thead>
        <tbody>
        @foreach ($classification as $user)
            <tr>
                <td>{{$user->current_position}}</td>
                <td>@if($user->current_position > $user->last_position)
                        {{ HTML::image("images/arrow_down.png", "Down") }}
                    @elseif($user->current_position < $user->last_position)
                        {{ HTML::image("images/arrow_up.png", "Up") }}
                    @else
                        {{ HTML::image("images/equal.png", "Equal") }}
                    @endif
                </td>
                <td><a  href="{{URL::to('detail/'.$user->id)}}">{{$user->name}}</a></td>
                <td>{{$user->points}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@stop