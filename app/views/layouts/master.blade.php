<!DOCTYPE html>
<html lang='en'>
    <head>
        <title>Porra</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        {{HTML::style('css/bootstrap.min.css')}}
        {{HTML::style('css/content.css')}}
        @yield('css')
    </head>
    <body>
    <div class="container">
        <nav class="navbar navbar-inverse row" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="{{URL::to('/')}}" class="navbar-brand">La Porra del mundial</a>
            </div>
        </nav>
    </div>
    <div class="container">
        @yield('content')
    </div>
    {{ HTML::script('js/jquery.min.js') }}
    {{ HTML::script('js/bootstrap.min.js') }}
    @yield('js')
    </body>
</html>