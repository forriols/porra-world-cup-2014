<?php

namespace Porra\Storage\Results;

interface InterfaceResultsRepository {

    public  function all();
    public  function calculatePoints($player_id);
    public  function getComingMatches();
    public  function updateResultLeagueMatches($value,$id);
}