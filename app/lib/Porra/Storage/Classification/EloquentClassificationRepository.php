<?php

namespace Porra\Storage\Classification;

use Classification;

/**
 * Class used as a wrapper for the Eloquent model redefining the public methods used in the
 * Controllers to access the model data for Classification.
 *
 * Class EloquentClassificationRepository
 * @package Porra\Storage\Classification
 */
class EloquentClassificationRepository implements InterfaceClassificationRepository {


    public function all()
    {
        return Classification::all();
    }

    public function getClassification($users)
    {
        return Classification::getClassification($users);
    }

    public function update($input)
    {

    }
}