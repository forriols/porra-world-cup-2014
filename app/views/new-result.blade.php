@extends('layouts.master')
@section('content')
{{ Form::open(array('url' => 'new-result','class'=>'form-horizontal','name'=>'form-new-result','onsubmit'=>'return true;','files'=>true)) }}

@foreach ($matches as $match)
<div>
    {{$match->description}}
    <select name="{{$match->id}}">
        <option selected="selected" value="-">-</option>
        <option value="1">1</option>
        <option value="x">x</option>
        <option value="2">2</option>
    </select>
</div>
<br>
@endforeach
<button class="btn-block btn-default btn-warning" type="submit">Envia</button>
{{Form::close()}}
@stop