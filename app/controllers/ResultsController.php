<?php

/**
 * Class ResultsController
 */
class ResultsController extends \BaseController {

    private $results;
    private $classification;
    private $players;

    /**
     * We inject the dependencies for this Controller. Each dependency is a repository which allow us to
     * retrieve data.
     *
     * @param Classification $classification
     * @param Results $results
     * @param Players $players
     */
    public function __construct(Classification $classification, Results $results, Players $players)
    {
        $this->results          = $results;
        $this->classification   = $classification;
        $this->players          = $players;
    }

    /**
     * Call all the methods needed to update the classification and get the points for each player.
     * After this call we get the classification up to date.
     */
    public function updateClassification()
    {
        $this->classification->updateLastPositions();
        $this->classification->updatePoints($this->players,$this->results);
        $this->classification->updateCurrentPositions();
    }

    /**
     * Show the form for update a new/news results for the league matches. We get the matches that are
     * coming to play.
     *
     * @return Response
     */
    public function newResult()
    {
        return View::make('new-result', array('matches' => $this->results->getComingMatches()));
    }


    /**
     * Store all the results that we got from POST call that have been set in the last form.
     * Finally, we update the classification and we show the home page.
     *
     * @return Response
     */
    public function storeResult()
    {
        foreach ($this->results->all() as $result) {
            if (Input::has($result->id)) {
                $this->results->updateResultLeagueMatches(Input::get($result->id),$result->id);
            }
        }
        $this->updateClassification();
        return Redirect::route('classification.home');
    }
}
