<?php

class Results extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'results';

    /**
     * This id will be used to get the points from this user.
     * Is important to assign a value to this attribute before calling the methods
     * of this class.
     *
     * @var int
     */
    protected $player_id;

    /**
     * We call this method for the results/bets that only need a simple comporation between the results'value
     * and bets'value
     *
     * @param $table_results
     * @param $table_bets
     * @return int return the total points for the user with id value specified in the
     * attribute @see $player_id
     */
    private function calculatePointsMatching($table_results, $table_bets)
    {
        $points = DB::table($table_results)
            ->join($table_bets, "$table_results.id",'=', "$table_bets.fk_id_results")
            ->whereRAW("$table_results.value = $table_bets.value")
            ->whereRAW("$table_bets.fk_id_users = $this->player_id")
            ->sum('points');

        return ($points) ? $points : 0;
    }

    /**
     * Get the amount of successful bets specified in a group of results.
     *
     * @param $results
     * @param $bets
     * @return int
     */
    private function getGroupScore($results,$bets) {
        $count   = 0;
        foreach ($bets as $bet) {
            if (in_array($bet,$results)) {
                $count ++;
            }
        }
        return $count;
    }

    /**
     * From all the bets each one specified as a group of data seperated by ','. We compare these values
     * to the group results to know how many succesful bets has the user guessed and calculate the points for
     * each correct bet.
     *
     * @param $table_results
     * @param $table_bets
     * @return int return the total points for the user with id value specified in the
     * attribute @see $player_id
     */
    private function calculatePointsGroup($table_results, $table_bets)
    {
        $bets = DB::table($table_results)
            ->join($table_bets,"$table_results.id",'=',"$table_bets.fk_id_results")
            ->whereRAW("$table_bets.fk_id_users = ". $this->player_id)
            ->select("$table_bets.value as bets_value","$table_results.value as results_value",
                     "$table_results.points as points")
            ->get();

        $points = 0;
        foreach ($bets as $bet) {
            $results_value  = explode(',',$bet->results_value);
            $bets_value     = explode(',',$bet->bets_value);
            $points += $this->getGroupScore($results_value,$bets_value) * $bet->points;
        }
        return $points;
    }

    /**
     * We call the simple comparation method with the tables for LEAGUE MATCHES.
     * @return int points
     */
    private function getPointsFromLeagueMatches()
    {
        return $this->calculatePointsMatching('results_league_matches','bets_league_matches');
    }

    /**
     * We use the group comparation method to discover how many points has the player with id $player_id
     * that has bets in the table bets_classified_teams
     *
     * @return int points
     */
    private function getPointsClassifiedTeams()
    {
        return $this->calculatePointsGroup('results_classified_teams','bets_classified_teams');
    }

    /**
     * We call the simple comparation method with the values for PAIRING MATCHES.
     * @return int points
     */
    private function getPointsPairingMatches()
    {
        return $this->calculatePointsGroup('results_pairing_matches','bets_pairing_matches');
    }

    /**
     * We call the simple comparation method with the values for Spain last round
     * @return int points
     */
    private function getPointsSpainLastRound()
    {
        return $this->calculatePointsMatching('results_spain_round','bets_spain_round');
    }

    /**
     * We call the simple comparation method with the values for best scorer
     * @return int points
     */
    private function getPointsBestScorer()
    {
        return $this->calculatePointsGroup('results_best_scorer','bets_best_scorer');
    }

    /**
     * saving player_id as an attribute
     * @param $player_id
     */
    public function setPlayerId($player_id)
    {
        $this->player_id = $player_id;
    }


    /**
     * We get all the points from the user with id player_id
     *
     * @param $player_id
     * @return int
     */
    public function calculatePoints($player_id)
    {
        $this->setPlayerId($player_id);

        if (!$this->player_id) {
            return 0;
        }

        $points = 0;
        //calculate points for group matches
        $points += $this->getPointsFromLeagueMatches();
        //calculate points for classified teams
        $points += $this->getPointsClassifiedTeams();
         //calculate points for round matches
        $points += $this->getPointsPairingMatches();
         //calculate points for best scorer
        $points += $this->getPointsBestScorer();
         //calculate points for spain last round
        $points += $this->getPointsSpainLastRound();

        return $points;
    }

    /**
     * Retrieve the matches from league matches that are waiting to play
     *
     * @return mixed
     */
    public static function getComingMatches()
    {

        return DB::table('results_league_matches')
            ->where('value','=','-')
            ->orderBy('dt_start','asc')
            ->get();
    }

    /**
     * Persistant action to save the new result
     *
     * @param $value
     * @param $result_id
     */
    public static function updateResultLeagueMatches($value,$result_id)
    {
        DB::table('results_league_matches')
            ->where('id', $result_id)
            ->update(array('value' => $value));
    }
}
