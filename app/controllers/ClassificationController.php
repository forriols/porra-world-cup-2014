<?php

/**
 * Class ClassificationController
 */
class ClassificationController extends \BaseController {

    private $classification;
    private $players;
    private $results;

    /**
     * We inject the dependencies for this Controller. Each dependency is a repository which allow us to
     * retrieve data.
     *
     * @param Classification $classification repository
     * @param Players $players repository
     * @param Results $results repository
     */
    public function __construct(Classification $classification, Players $players, Results $results)
    {
        $this->classification   = $classification;
        $this->players          = $players;
        $this->results          = $results;
    }

    /**
     * Return html-title
     *
     * @return string
     */
    private function getTitle()
    {
        return 'Home - Porra del mundial';
    }

	/**
	 * Display a listing of the resource classification.
	 *
	 * @return Response
	 */
	public function showClassification()
	{
        return View::make('home', array('classification'    => $this->classification->getClassification(),
                                        'title'             => $this->getTitle()));
	}
}
