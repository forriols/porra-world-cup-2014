<?php

namespace Porra\Storage;

use Illuminate\Support\ServiceProvider;

/**
 * Using this class to provide our custom class for the different Repositories.
 *
 * Class StorageServiceProvider
 * @package Porra\Storage
 */
class StorageServiceProvider extends ServiceProvider {

    public function register()
    {
        $this->app->bind(
            'Porra\Storage\Classification\InterfaceClassificationRepository',
            'Porra\Storage\Classification\EloquentClassificationRepository'
        );
        $this->app->bind(
            'Porra\Storage\Players\InterfacePlayersRepository',
            'Porra\Storage\Players\EloquentPlayersRepository'
        );
        $this->app->bind(
            'Porra\Storage\Results\InterfaceResultsRepository',
            'Porra\Storage\Results\EloquentResultsRepository'
        );
    }
}