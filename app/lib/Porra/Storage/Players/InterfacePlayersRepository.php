<?php

namespace Porra\Storage\Players;

interface InterfacePlayersRepository {

    public function all();

    public function calculatePoints($player);
}